LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'basic_with_time': {
            'format': '%(levelname)s - %(asctime)s - %(name)s - %(message)s'
        }
    },
    'handlers': {
        'std_out_info': {
            'level': 'INFO',
            'formatter': 'basic_with_time',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
    },
    'loggers': {
        'parser_logger': {
            'handlers': ['std_out_info'],
            'level': 'INFO',
        }
    }
}
