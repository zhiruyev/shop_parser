import logging
import time
import re
from logging.config import dictConfig
from decimal import Decimal

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

from config import LOGGING_CONFIG

dictConfig(LOGGING_CONFIG)

logger = logging.getLogger('parser_logger')
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))


def get_product_price(product_link: str) -> Decimal:
    logger.info(f'received link: {product_link}')
    driver.get(product_link)
    logger.info(f'got content for link: {product_link}')

    almaty_city = driver.find_element(By.XPATH, "//*[@data-city-id='750000000']")
    almaty_city.click()

    logger.info(f'choosed city for: {product_link}')
    time.sleep(1)

    price = driver.find_element(By.CLASS_NAME, 'item__price-once').get_attribute('innerText')
    logger.info(f'parsed price: {product_link}')

    price = re.sub('\D', '', price)

    logger.info(f'price for {product_link} is {price}')
    driver.quit()

    return Decimal(price)


get_product_price('https://kaspi.kz/shop/p/apple-iphone-15-pro-max-256gb-dual-sim-belyi-113587645/?c=750000000')
